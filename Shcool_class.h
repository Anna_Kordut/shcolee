class Shcool_class {
private:
    int number_class;
    char letter;
    int coll_people;
public:
    int getNumberClass() const;

    void setNumberClass(int numberClass);

    char getLetter() const;

    void setLetter(char letter);

    int getCollPeople() const;

    void setCollPeople(int collPeople);
};

