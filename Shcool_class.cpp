#include "Shcool_class.h"

int Shcool_class::getNumberClass() const {
    return number_class;
}

void Shcool_class::setNumberClass(int numberClass) {
    number_class = numberClass;
}

char Shcool_class::getLetter() const {
    return letter;
}

void Shcool_class::setLetter(char letter) {
    Shcool_class::letter = letter;
}

int Shcool_class::getCollPeople() const {
    return coll_people;
}

void Shcool_class::setCollPeople(int collPeople) {
    coll_people = collPeople;
}
